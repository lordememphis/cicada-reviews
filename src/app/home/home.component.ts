import { Component, OnInit } from '@angular/core';
import { ApiService, Game } from '../data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [],
})
export class HomeComponent implements OnInit {
  game!: Game;

  constructor(private readonly api: ApiService) {}

  ngOnInit(): void {
    this.game = this.getRandomGame();
  }

  getRandomGame(): Game {
    const rand = Math.floor(Math.random() * 18);
    return this.api.games.find(
      (game) => this.api.games.indexOf(game) === rand
    )!;
  }
}
