import { Injectable } from '@angular/core';
import gamesData from 'src/assets/data/games.data.json';
import { Review, ReviewInfo } from '.';
import { Game } from './game.interface';

@Injectable({ providedIn: 'root' })
export class ApiService {
  get games(): Game[] {
    return gamesData as unknown as Game[];
  }

  getGame(id: string): Game {
    return gamesData.find((game) => game.id === +id) as unknown as Game;
  }

  addGameReview(gameId: string, review: ReviewInfo): void {
    let reviews: Review[] = [];
    if (localStorage.getItem('reviews')) {
      reviews = JSON.parse(localStorage.getItem('reviews')!) as Review[];
      const gameReviews = reviews.find(
        (review) => review.gameId === +gameId
      ) as Review;
      if (gameReviews) {
        gameReviews.reviews.push(review);
      } else {
        reviews.push({ gameId: +gameId, reviews: [review] });
      }
    } else {
      reviews.push({ gameId: +gameId, reviews: [review] });
    }

    localStorage.setItem('reviews', JSON.stringify(reviews));
  }

  getGameReviews(gameId: string): Review {
    const reviews = JSON.parse(localStorage.getItem('reviews')!) as Review[];
    return reviews.find((review) => review.gameId === +gameId) as Review;
  }
}
