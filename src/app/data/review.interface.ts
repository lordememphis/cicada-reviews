export interface Review {
  gameId: number;
  reviews: ReviewInfo[];
}

export interface ReviewInfo {
  createdAt: Date;
  review: string;
}
