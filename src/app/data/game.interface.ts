export interface Game {
  id: number;
  slug: string;
  name: string;
  released: string;
  tba: boolean;
  background_image: string;
  rating: number;
  rating_top: number;
  ratings: GameRating[];
  ratings_count: number;
  reviews_text_count: number;
  added: number;
  added_by_status: GameAddedByStatus;
  metacritic: number;
  playtime: number;
  suggestions_count: number;
  updated: string;
  user_game: unknown;
  reviews_count: number;
  saturated_color: string;
  dominant_color: string;
  platforms: GamePlatform[];
  parent_platforms: GameParentPlatform[];
  genres: GameGenre[];
  stores: GameStore[];
  clip: string;
  tags: GameTag[];
  esrb_rating: GameEsrbRating;
  short_screenshots: GameShortScreenshot[];
}

export interface GameRating {
  id: number;
  title: string;
  count: number;
  percent: number;
}

export interface GameAddedByStatus {
  yet: number;
  owned: number;
  beaten: number;
  toplay: number;
  dropped: number;
  playing: number;
}

export interface GamePlatform {
  platform: GamePlatformInfo;
  released_at: string;
  requirements_en: unknown;
  requirements_ru: unknown;
}

interface GamePlatformInfo {
  id: number;
  name: string;
  slug: string;
  image: string;
  year_end: number;
  year_start: number;
  games_count: number;
  image_background: string;
}

export interface GameParentPlatform {
  platform: GameParentPlatformInfo;
}

interface GameParentPlatformInfo {
  id: number;
  name: string;
  slug: string;
}

export interface GameGenre {
  id: number;
  name: string;
  slug: string;
  games_count: number;
  image_background: string;
}

export interface GameStore {
  id: number;
  store: GameStoreInfo;
}

interface GameStoreInfo {
  id: number;
  name: string;
  slug: string;
  domain: string;
  games_count: number;
  image_background: string;
}

export interface GameTag {
  id: number;
  name: string;
  slug: string;
  language: string;
  games_count: number;
  image_background: string;
}

export interface GameEsrbRating {
  id: number;
  name: string;
  slug: string;
}

export interface GameShortScreenshot {
  id: number;
  image: string;
}
