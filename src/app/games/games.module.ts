import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './detail/game.component';
import { GamesComponent } from './list/games.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [GameComponent, GamesComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: GamesComponent },
      { path: ':id', component: GameComponent },
    ]),
    ReactiveFormsModule,
  ],
})
export class GamesModule {}
