import { Component } from '@angular/core';
import { ApiService } from 'src/app/data';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styles: [
    `
      .bg {
        background: url('https://i1.wp.com/overmental.com/wp-content/uploads/2015/08/godzilla-film-2014.jpg?fit=2560%2C1440&ssl=1');
      }
    `,
  ],
})
export class GamesComponent {
  games = this.api.games;

  constructor(private readonly api: ApiService) {}
}
