import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService, Game, Review, ReviewInfo } from 'src/app/data';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styles: [],
})
export class GameComponent implements OnInit {
  game!: Game;
  reviews!: Review;

  reviewForm!: FormGroup;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: ApiService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.game = this.api.getGame(params['id']);
        this.reviews = this.api.getGameReviews(params['id']);
      }
    });

    this.reviewForm = new FormGroup({
      review: new FormControl(null, Validators.required),
    });
  }

  submitReview(gameId: number): void {
    const review: ReviewInfo = {
      createdAt: new Date(),
      review: this.reviewForm.get('review')?.value,
    };
    this.api.addGameReview(String(gameId), review);
    this.router
      .navigateByUrl('/')
      .then(() => this.router.navigate(['game', gameId]));
  }
}
