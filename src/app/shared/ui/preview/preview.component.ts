import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
})
export class PreviewComponent {
  @Input() routerUrl!: string;
  @Input() imageUrl!: string;
  @Input() title!: string;
  @Input() year!: string;
  @Input() rating!: number;
  @Input() voteCount!: number;
}
