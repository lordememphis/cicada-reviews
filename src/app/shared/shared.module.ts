import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreviewComponent } from './ui/preview/preview.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [PreviewComponent],
  imports: [CommonModule, RouterModule],
  exports: [PreviewComponent],
})
export class SharedModule {}
