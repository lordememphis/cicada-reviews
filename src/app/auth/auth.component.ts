import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
})
export class AuthComponent implements OnInit {
  createAccount = false;
  form!: FormGroup;

  constructor(private readonly authService: AuthService) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  auth(): void {
    if (this.form.valid) {
      if (this.createAccount) {
        if (this.authService.createAccount(this.form.value)) {
          alert('Account created, successfully. Now login');
          this.createAccount = false;
        }
      } else {
        this.authService.login(this.form.value);
      }
    } else {
      alert('Enter username and password');
    }
    this.form.reset();
  }
}
