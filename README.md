### How to run app

Make sure you have node installed on you machine. If not get it [here](https://nodejs.org/en/download/) and install the one that corresponds to your operaing system.

#### NB: You can only continue to the next steps if node is installed.

##### Install app dependencies

In project root directory run `npm install`

Once all dependencies are successfully installed,

##### Start app

In project root directory run `npm start`

App will automatically open in default web browser on port `4200`
